package main.java.config;

/**
 * @author lionel.mangoua
 * Date: 30/01/22
 */

import main.java.engine.DriverFactory;

public class GlobalEnums extends DriverFactory {

    static String novoFxBetaAppName = "NovoFx Beta";
//    static String novoFxMockAppName = "NovoFx Mock";
//    static String novoFxMockDebugApp = "mock-debug";
//    static String novoFxMockReleaseApp = "mock-release";
//    static String novoFxBetaDebugApp = "beta-debug";
//    static String novoFxBetaReleaseApp = "beta-release";

    //region <MOBILE>
    public enum Devices {

        PIXEL_3_XL_EMULATOR("Pixel 3 XL API 29", "10.0", "emulator-5554"),
        PIXEL_3_EMULATOR("Pixel 3 API 28", "9.0", "emulator-5556"),
        HUAWEI_P30_LITE_DEVICE("Huawei P30 Lite", "10.0", "xxxxx"),
        IPHONE_12_PRO_EMULATOR("iPhone 12 Pro", "14.5", "63EA9FA6-E0E6-4E4D-8537-AA3351EBCEA0"),
        IPHONE_12_PRO_MAX_EMULATOR("iPhone 12 Pro Max", "14.5", "25D34633-A36E-4E6E-AD14-A089CD9B900B"); //2D7D0E52-49C6-4993-85A2-50596A7560F8

        public final String deviceName;
        public final String platformVersion;
        public final String udid;

        Devices(String deviceName, String platformVersion, String udid) {
            this.deviceName = deviceName;
            this.platformVersion = platformVersion;
            this.udid = udid;
        }
    }

    public enum AppInfo {

        NOVOFX_BETA_APP(novoFxBetaAppName, "za.co.absa.novofx.beta", "za.co.absa.novofx.ui.activities.LaunchActivity");

        public final String appName;
        public final String appPackage;
        public final String appActivity;

        AppInfo(String appName, String appPackage, String appActivity) {
            this.appName = appName;
            this.appPackage = appPackage;
            this.appActivity = appActivity;
        }
    }
    //endregion
}

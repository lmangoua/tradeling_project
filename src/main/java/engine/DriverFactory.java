package main.java.engine;

/**
 * @author lionel.mangoua
 * date: 26/01/22
 */

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.remote.AutomationName;
import io.appium.java_client.remote.MobileCapabilityType;
import io.github.bonigarcia.wdm.WebDriverManager;
import main.java.config.GlobalEnums;
import main.java.utils.PropertyFileReader;
import main.java.utils.SeleniumUtility;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.Platform;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import java.io.File;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class DriverFactory {

    public static PropertyFileReader property = new PropertyFileReader();
    public static String web_fileName = "WEB";
    public static String mobi_fileName = "MOBILE";
    private static boolean root = false;
    public static String testName;
    public static GlobalEnums.Devices deviceType;
    public static GlobalEnums.AppInfo appInfo;

    //Web
    public static RemoteWebDriver driver;
    public static WebDriverWait wait = null;
    public static final int WAIT_TIME = 30;
    public static String BROWSER = System.getProperty("BROWSER");
    public static ChromeOptions options;
    public static final Logger LOGGER = LoggerFactory.getLogger(DriverFactory.class);
    public static String url = property.returnPropVal_web(web_fileName, "url");
    //Mobile
    public static AppiumDriver Driver; //For appium
    public static DesiredCapabilities capabilities; //Android
    public static DesiredCapabilities iosCapabilities; //iOS
    public static String Execution_Mobile = System.getProperty("EXECUTION_MOBILE");
    public static String OperatingSystem = System.getProperty("OS");
    public static String appPath = "src/main/resources/mobile/xxx.apk"; //For mobile app
    public static String Device = System.getProperty("DEVICE"); //For the type of device setup in the task
    public static String appAbsolutePath = "";
    public static String label = property.returnPropVal_mobi(mobi_fileName, "label");
    public static String packName = System.getProperty("REGRESSION_PACK");
    public static String Host = System.getProperty("HOST");
    public static String Port = "4723"; //4723 by default
    /*Android App*/
    public static String novoFxBetaDebugAndroidAppPath = property.returnPropVal_mobi(mobi_fileName, "novoFxBetaDebugAndroidAppPath"); //For NovoFX Beta Debug android app
    public static String novoFxBetaReleaseAndroidAppPath = property.returnPropVal_mobi(mobi_fileName, "novoFxBetaReleaseAndroidAppPath"); //For NovoFX Beta Release android app
    public static String novoFxMockDebugAndroidAppPath = property.returnPropVal_mobi(mobi_fileName, "novoFxMockDebugAndroidAppPath"); //For NovoFX Mock Debug android app
    public static String novoFxMockReleaseAndroidAppPath = property.returnPropVal_mobi(mobi_fileName, "novoFxMockReleaseAndroidAppPath"); //For NovoFX Mock Release android app
    /*iOS App*/
    public static String novoFxBetaDebugIosAppPath = property.returnPropVal_mobi(mobi_fileName, "novoFxBetaDebugIosAppPath"); //For NovoFX Beta Debug ios app

    @BeforeSuite(alwaysRun = true)
    public void initialiseDriverAndProperties() {

        setupLocalDriver();
    }

    //WEB
    //region <setupLocalDriver>
    public static void setupLocalDriver() {

        log("\nWeb test is Starting ... \n","INFO",  "text");

        if("Firefox".equals(BROWSER)) {
            //setup the firefoxdriver using WebDriverManager dependency
            WebDriverManager.firefoxdriver().setup();

            FirefoxOptions firefoxOptions = new FirefoxOptions();

            driver = new FirefoxDriver(firefoxOptions);
        }
        else if("Chrome".equals(BROWSER)) {
            //setup the chromedriver using WebDriverManager dependency
            WebDriverManager.chromedriver().setup();

            options = new ChromeOptions();

            options.addArguments("--disable-extensions");
            options.addArguments("disable-infobars");
            options.addArguments("test-type");
            options.addArguments("enable-strict-powerful-feature-restrictions");
            options.addArguments("--disable-popup-bloacking");
            options.addArguments("--dns-prefetch-disable");
            options.addArguments("--safebrowsing-disable-auto-update");
            options.addArguments("disable-restore-session-state");
            options.addArguments("start-maximized"); //maximize browser
            options.setCapability(ChromeOptions.CAPABILITY, options);
            options.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);

            driver = new ChromeDriver(options);
        }

        log("\n'" + BROWSER + "' browser on local machine initiated \n","INFO",  "text");

        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, WAIT_TIME);
    }
    //endregion

    //Mobile Setup
    //region <openMobileApp>
    public void openMobileApp(String platform, String appPath, String appType) throws Exception {

        switch(Execution_Mobile.toLowerCase()) {
            case("remote"):
                break;
            case("local"):
            default:
                if(platform.equalsIgnoreCase("Android")) {
                    createLocalAndroidDriver(platform, appPath, appType); //For Android
                }
                else if(platform.equalsIgnoreCase("iOS")) {
                    createLocalIOSDriver(platform, appPath, appType); //For iOS
                }
                else {
                    log("WARNING NO PLATFORM WAS SELECTED!!! \n", "ERROR", "text");
                }
                break;
        }

        Driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        wait = new WebDriverWait(Driver, WAIT_TIME);
    }
    //endregion

    //region <createLocalAndroidDriver>
    public void createLocalAndroidDriver(String platform, String appPath, String appType) {

        try {
            log("Mobile test is Starting ... \n", "INFO", "text");

            File appFile = new File(appPath);

            log("Local App driver directory - " + appFile.getAbsolutePath(), "INFO", "text");
            appAbsolutePath = appFile.getAbsolutePath();

            capabilities = new DesiredCapabilities();

            capabilities.setCapability("name", testName);
            capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, Platform.ANDROID);
            capabilities.setCapability(MobileCapabilityType.APP, appAbsolutePath + "");
            if(Device.equalsIgnoreCase("emulator")) {
                log("Device is: '" + Device + "'", "INFO", "text");
                deviceType = GlobalEnums.Devices.PIXEL_3_XL_EMULATOR; //Set device type
                //Set device info.
                capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceType.deviceName);
                capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, deviceType.platformVersion);
                capabilities.setCapability(MobileCapabilityType.UDID, deviceType.udid); //Android Studio emulator
            }
            else if(Device.equalsIgnoreCase("huawei")) {
                log("Device is: '" + Device + "'", "INFO", "text");
                deviceType = GlobalEnums.Devices.HUAWEI_P30_LITE_DEVICE; //Set device type - Real device
                //Set device info.
                capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceType.deviceName);
                capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, deviceType.platformVersion);
                capabilities.setCapability(MobileCapabilityType.UDID, deviceType.udid);
            }
            //Check which App is it
            if(appType.equalsIgnoreCase("Beta")) {
                appInfo = GlobalEnums.AppInfo.NOVOFX_BETA_APP; //Set device type
                capabilities.setCapability("appPackage", appInfo.appPackage); //search for 'Package name:' in appium logs
                capabilities.setCapability("appActivity", appInfo.appActivity); //search for 'Main activity name:' in appium logs
            }
            else if(appType.equalsIgnoreCase("Mock")) {
//                appInfo = GlobalEnums.AppInfo.NOVOFX_MOCK_APP; //Set device type
//                capabilities.setCapability("appPackage", appInfo.appPackage); //search for 'Package name:' in appium logs
//                capabilities.setCapability("appActivity", appInfo.appActivity); //search for 'Main activity name:' in appium logs
            }
            capabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.ANDROID_UIAUTOMATOR2);
            capabilities.setCapability(MobileCapabilityType.APPIUM_VERSION, "1.18.3");
            capabilities.setCapability(MobileCapabilityType.NO_RESET, false); //To reset app before each session
            capabilities.setCapability("newCommandTimeout", 15); //To make test cases fail fast in order to quickly get an error message
            capabilities.setCapability("autoAcceptAlerts", true); //To accept popup alerts
            capabilities.setCapability("autoGrantPermissions", true); //To accept popup alerts
            capabilities.setCapability(MobileCapabilityType.SUPPORTS_ALERTS, true); //To accept popup alerts
            capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 700); //For the App to go to sleep after 700s
            capabilities.setCapability("ignoreUnimportantViews", true); //helps to reduce the time of inputs on Android
            capabilities.setCapability("disableAndroidWatchers", true); //helps to reduce the time of inputs on Android
            capabilities.setCapability("unicodeKeyboard", true); //To remove virtual keyboard

            log("Driver = " + Host + ":" + Port + "/wd/hub \n", "INFO", "text"); //Appium default port 0.0.0.0:4723

            Driver = new AndroidDriver(new URL("http://" + Host + ":" + Port + "/wd/hub"), capabilities);
            log(platform + " on local machine initiated \n", "INFO", "text"); //Setup Android Virtual Device's name
        }
        catch (Exception e) {
            log("Failed to setup Local Mobile Driver --- " + e.getMessage(), "ERROR", "text");
        }
    }
    //endregion

    //region <createLocalIOSDriver>
    public void createLocalIOSDriver(String platform, String appPath, String appType) {

        try {
            log("Mobile test is Starting ... \n", "INFO", "text");

            File appFile = new File(appPath);

            log("Local App driver directory - " + appFile.getAbsolutePath(), "INFO", "text");
            appAbsolutePath = appFile.getAbsolutePath();

            iosCapabilities = new DesiredCapabilities();

            iosCapabilities.setCapability("name", testName);
            iosCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, Platform.IOS);
            iosCapabilities.setCapability(MobileCapabilityType.APP, appAbsolutePath + "");
            //Check if it is "emulator" or "real device"
            if(Device.equalsIgnoreCase("emulator")) {
                log("Device is: '" + Device + "'", "INFO", "text");
                deviceType = GlobalEnums.Devices.IPHONE_12_PRO_MAX_EMULATOR; //Set device type
//                deviceType = GlobalEnums.Devices.IPHONE_12_PRO_EMULATOR; //Set device type //TODO works
//                deviceType = GlobalEnums.Devices.IPHONE_11_PRO_MAX_EMULATOR; //Set device type
                //Set device info.
                iosCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceType.deviceName);
                iosCapabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, deviceType.platformVersion);
                iosCapabilities.setCapability(MobileCapabilityType.UDID, deviceType.udid); //Xcode emulator
            }
            else if(Device.equalsIgnoreCase("iPhone")) {
                log("Device is: '" + Device + "'", "INFO", "text");
//                deviceType = GlobalEnums.Devices.IPHONE_6_S_DEVICE; //Set device type - Real device
//                //Set device info.
//                iosCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceType.deviceName);
//                iosCapabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, deviceType.platformVersion);
//                iosCapabilities.setCapability(MobileCapabilityType.UDID, deviceType.udid); //Xcode emulator
            }
            iosCapabilities.setCapability(MobileCapabilityType.AUTOMATION_NAME, AutomationName.IOS_XCUI_TEST);
            iosCapabilities.setCapability(MobileCapabilityType.APPIUM_VERSION, "1.18.3");
            iosCapabilities.setCapability(MobileCapabilityType.NO_RESET, false); //To reset app before each session
            iosCapabilities.setCapability("newCommandTimeout", 15); //To make test cases fail fast in order to quickly get an error message
            iosCapabilities.setCapability("autoAcceptAlerts", false); //To accept popup alerts
//            iosCapabilities.setCapability("autoAcceptAlerts", true); //To accept popup alerts //TODO del
            iosCapabilities.setCapability("autoGrantPermissions", true); //To accept popup alerts
            iosCapabilities.setCapability(MobileCapabilityType.SUPPORTS_ALERTS, true); //To accept popup alerts
            iosCapabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 700); //For the App to go to sleep after 700s
            iosCapabilities.setCapability("ignoreUnimportantViews", true); //helps to reduce the time of inputs on Android
            iosCapabilities.setCapability("disableAndroidWatchers", true); //helps to reduce the time of inputs on Android
            iosCapabilities.setCapability("unicodeKeyboard", true); //To remove virtual keyboard
            iosCapabilities.setCapability("resetKeyboard", true); //To hide virtual keyboard
            iosCapabilities.setCapability("wdaLocalPort", "8100"); //for Mac only

            log("Driver = " + Host + ":" + Port + "/wd/hub \n", "INFO", "text"); //Appium default port 0.0.0.0:4723

            Driver = new IOSDriver(new URL("http://" + Host + ":" + Port + "/wd/hub"), iosCapabilities);
            log(platform + " on local machine initiated \n", "INFO", "text"); //Setup Android Virtual Device's name
        }
        catch(Exception e) {
            log("Failed to setup Local Mobile Driver --- " + e.getMessage(), "ERROR", "text");
        }
    }
    //endregion

    //region <createLocalDriver>
//    public void createLocalDriver(String appName) {
//
//        try {
//            log("Mobile test is Starting ... \n", "INFO", "text");
//
//            File appFile = new File(appName);
//
//            log("Local App driver directory - " + appFile.getAbsolutePath(), "INFO", "text");
//            appAbsolutePath = appFile.getAbsolutePath();
//
//            capabilities = new DesiredCapabilities();
//
//            capabilities.setCapability("name", testName);
//            capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, Platform.ANDROID);
//            capabilities.setCapability(MobileCapabilityType.APP, appAbsolutePath + "");
//            if(DEVICE.equalsIgnoreCase("emulator")) {
//                log("Device is: '" + DEVICE +"'", "INFO", "text");
//                deviceType = GlobalEnums.Devices.NEXUS_6_EMULATOR; //Set device type
//                capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceType.deviceName);
//                capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, deviceType.platformVersion);
//                capabilities.setCapability(MobileCapabilityType.UDID, deviceType.udid); //Android Studio emulator
//                capabilities.setCapability("unicodeKeyboard", true); //To remove virtual keyboard
//            }
//            else if(DEVICE.equalsIgnoreCase("driver palm")) {
//                log("Device is: '" + DEVICE +"'", "INFO", "text");
//                deviceType = GlobalEnums.Devices.DRIVER_PALM; //Set device type
//                capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, deviceType.deviceName);
//                capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, deviceType.platformVersion);
//                capabilities.setCapability(MobileCapabilityType.UDID, deviceType.udid); //Driver Palm Device emulator  other_device = 53edbe9b or 255b5a02
//                capabilities.setCapability("unicodeKeyboard", true); //To remove virtual keyboard
//            }
//            capabilities.setCapability(MobileCapabilityType.APPIUM_VERSION, "1.9.1");
//            capabilities.setCapability(MobileCapabilityType.NO_RESET, false); //To reset app before each session
//            capabilities.setCapability("newCommandTimeout", 15); //To make test cases fail fast in order to quickly get an error message
//            capabilities.setCapability("autoAcceptAlerts", true); //To accept popup alerts
//            capabilities.setCapability("autoGrantPermissions", true); //To accept popup alerts
//            capabilities.setCapability(MobileCapabilityType.SUPPORTS_ALERTS, true); //To accept popup alerts
//            capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 700); //For the App to go to sleep after 700s
//            capabilities.setCapability("ignoreUnimportantViews", true); //helps to reduce the time of inputs on Android
//            capabilities.setCapability("disableAndroidWatchers", true); //helps to reduce the time of inputs on Android
//
//            log("Driver = " + HOST + ":4723/wd/hub \n", "INFO", "text"); //Appium default port 0.0.0.0:4723
//
//            Driver = new AndroidDriver(new URL("http://" + HOST + ":4723/wd/hub"), capabilities);
//            log(OperatingSystem + " on local machine initiated \n", "INFO", "text"); //Setup Android Virtual Device's name
//        }
//        catch (Exception e) {
//            log("Failed to setup Local Mobile Driver --- " + e.getMessage(), "ERROR", "text");
//        }
//    }
    //endregion

    //region <logger>
    public static void logger(final String message, final String level, String format) {

        if(format.equalsIgnoreCase(("json"))) {
            String json = (new JSONObject(message)).toString(4); //To convert into pretty Json format
            LOGGER.info("\n" + json); //To print on the console
        }
        else {
            LOGGER.info(message); //To print on the console
        }
    }

    public static void log(final String message, final String level, String format) {

        try {
            logger(message, level, format);
        }
        catch (JSONException err) {
            logger(message, level, "text");
        }
    }
    //endregion

    @AfterSuite(alwaysRun = true)
    //region <tearDown>
    public void tearDown() {

        try {
            if(driver != null) {
                //To check if the browser is opened
                LOGGER.info("Test - WEB is Ending ...\n");
                SeleniumUtility.attachScreenShot("Tear down screenshot - Web");
                driver.quit();
            }

            if(Driver != null) {
                //To check if the mobile app is opened
                LOGGER.info("Test - MOBILE is Ending ...\n");
                SeleniumUtility.attachScreenShot("Tear down screenshot - Mobile");
                Driver.closeApp(); //Close the app which was provided in the capabilities at session creation
                Driver.quit(); //Quit the session created between the client and the server
            }
        }
        catch(Exception ex) {
            log("Something went wrong on test suite tear down!!! ---> " + ex, "INFO", "text");
        }
    }
    //endregion
}

package main.java.utils;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import main.java.engine.DriverFactory;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static io.appium.java_client.touch.LongPressOptions.longPressOptions;
import static io.appium.java_client.touch.offset.ElementOption.element;
import static io.appium.java_client.touch.offset.PointOption.point;
import static java.time.Duration.ofSeconds;

public class AppiumUtility extends DriverFactory {

    private static int WaitTimeout = 6;

    //region <activateApp>
    /**
     * Activate App running in the Background
     * to come back to Forefront
     */
    public static void activateApp(String appPackageName) {

        try {
            Driver.activateApp(appPackageName);

            System.out.println("Activate App successfully");
            Assert.assertTrue("Activate App successfully", true);
        }
        catch (Exception e) {
            log("Failed to Activate App", "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to Activate App --- " + e.getMessage());
        }
    }
    //endregion

    //region <clearTextAndEnterValue>
    public static void clearTextAndEnterValue(By element, String textToEnter, String errorMessage) {

        try {
            waitForElement(element, errorMessage);
            WebElement elementToTypeIn = Driver.findElement(element);
            elementToTypeIn.click();
            elementToTypeIn.clear();
            elementToTypeIn.click();
            Actions typeText = new Actions(Driver);
            typeText.moveToElement(elementToTypeIn);
            typeText.click(elementToTypeIn);
            typeText.sendKeys(elementToTypeIn, textToEnter);
            typeText.click(elementToTypeIn);
            typeText.perform();

            elementToTypeIn = Driver.findElement(element);

            elementToTypeIn.click();

            String retrievedText = elementToTypeIn.getAttribute("value");

            if (retrievedText != null) {

                if (retrievedText.equals(textToEnter)) {
                    System.out.println("Text entered matches text retrieved." + "  Text entered = " + textToEnter + "  : Text retrieved = " + retrievedText);
                    Assert.assertTrue("Text entered matches text retrieved." + "  Text entered = " + textToEnter + "  : Text retrieved = " + retrievedText, true);
                }
                else {
                    System.out.println("Text entered does not match text retrieved." + "  Text entered = " + textToEnter + "  : Text retrieved = " + retrievedText);
                    Assert.assertTrue("Text entered matches text retrieved." + "  Text entered = " + textToEnter + "  : Text retrieved = " + retrievedText, true);
                }
            }
            else {
                System.out.println("Null value Found");
                Assert.fail("\n[ERROR] Null value Found ---  ");
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to enter text : \"" + textToEnter + "\" by Xpath  ---  " + element + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <clearText>
    public static void clearText(By element, String errorMessage) {

        try {
            waitForElement(element, errorMessage);
            WebElement elementToTypeIn = Driver.findElement(element);
            elementToTypeIn.clear();

            log("Cleared text on element : " + element, "INFO",  "text");
            Assert.assertTrue("Cleared text on element : " + element, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to clear text on element --- " + element + " - " + e.getMessage());
        }
    }
    //endregion

    //region <clearAndEnterTextWithAdb>
    /**
     * Clear text field then enter text using "adb" which is faster than using senKeys
     */
    public static void clearAndEnterTextWithAdb(By element, String textToEnter, String uuid, String errorMessage) {

        try {
            waitForElement(element, errorMessage);
            WebElement elementToTypeIn = Driver.findElement(element);
            elementToTypeIn.click();
            elementToTypeIn.clear();
            new ProcessBuilder(new String[]{"adb", "-s", uuid, "shell", "input", "text", textToEnter})
                    .redirectErrorStream(true)
                    .start();

            System.out.println("Entered text: \"" + textToEnter + "\" to : " + element);
            Assert.assertTrue("Entered text: \"" + textToEnter + "\" to : " + element, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to clear field and enter text: \"" + textToEnter + "\" ---  " + element + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <clickAndroidKeyBoard>
    /**
     * This dynamic method allows you send Android KeyEvent. Such as:
     * AndroidKey.ENTER, AndroidKey.TAB, AndroidKey.SEARCH
     */
    public static void clickAndroidKeyBoard(KeyEvent keyEvent, String errorMessage) {

        try {
            AndroidDriver androidDriver = Driver;
            androidDriver.pressKey(keyEvent);
            Assert.assertTrue("Pressed Android KeyEvent: \"" + keyEvent + "\"", true);
        }
        catch(Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to press KeyEvent --- " + e.getMessage());
        }
    }
    //endregion

    //region <clickElement>
    public static void clickElement(By element, String errorMessage) {

        try {
            waitForElement(element, errorMessage);
            WebDriverWait wait = new WebDriverWait(Driver, WaitTimeout);
            wait.until(ExpectedConditions.elementToBeClickable(element));
            WebElement elementToClick = Driver.findElement(element);
            elementToClick.click();

            System.out.println("Clicked element : " + element);
            Assert.assertTrue("Clicked element : " + element, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to click on element --- " + element + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <clickElementByXpath>
    public static void clickElementByXpath(String elementXpath, String errorMessage) {

        try {
            waitForElementByXpath(elementXpath, errorMessage);
            WebDriverWait wait = new WebDriverWait(Driver, WaitTimeout);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
            WebElement elementToClick = Driver.findElement(By.xpath(elementXpath));
            elementToClick.click();

            System.out.println("Clicked element by Xpath : " + elementXpath);
            Assert.assertTrue("Clicked element by Xpath : " + elementXpath, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to click on element by Xpath  ---  " + elementXpath + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <clickOptionByXpath>
    /*Dynamic method to click an element by Xpath. Using a dynamic Xpath of the element, we just specify the value to click*/
    public static void clickOptionByXpath(String element, String valueToSelect, String errorMessage) {

        String updatedXpath = "";

        try {
            updatedXpath = updateXpathValueWithString(element, valueToSelect); //update default value saved in .properties

            waitForElementByXpath(updatedXpath, errorMessage);
            WebDriverWait wait = new WebDriverWait(Driver, WaitTimeout);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(updatedXpath)));
            WebElement elementToClick = Driver.findElement(By.xpath(updatedXpath));
            elementToClick.click();

            System.out.println("Clicked element by Xpath : " + updatedXpath);
            Assert.assertTrue("Clicked element by Xpath : " + updatedXpath, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to click on element by Xpath --- " + updatedXpath + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <doubleClickElement>
    public static void doubleClickElement(By element, String errorMessage) {

        try {
            waitForElement(element, errorMessage);
            Actions act = new Actions(Driver);
            WebElement elementToClick = Driver.findElement(element);

            act.doubleClick(elementToClick).perform();
            System.out.println("Double-clicked element : " + element);
            Assert.assertTrue("Double-clicked element : " + element, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to double click element  ---  " + element + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <drawStraightLine>
    public static void drawStraightLine(String errorMessage) {

        try {
            //Getting screen size
            Dimension dim = Driver.manage().window().getSize();

            Double scrollHeightStart = dim.getHeight() * 0.5;
            int scrollStart = scrollHeightStart.intValue();

            Double scrollHeightEnd = dim.getHeight() * 0.2;
            int scrollEnd = scrollHeightEnd.intValue();

            TouchAction action = new TouchAction(Driver);

            action.press(point(25, scrollStart));
            action.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2)));
            action.moveTo(point(60, scrollEnd));
            action.release();
            action.perform();

            System.out.println("Drew a straight line --------------- ");
            Assert.assertTrue("Drew a straight line --------------- ", true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to draw a straight line  ---  " + e.getMessage());
        }
    }
    //endregion

    //region <drawStraightLongLine>
    public static void drawStraightLongLine(String errorMessage) {

        try {
            //Getting screen size
            Dimension dim = Driver.manage().window().getSize();

            Double scrollHeightStart = dim.getHeight() * 0.5; //1196.0
            int scrollStart = scrollHeightStart.intValue(); //1196

            Double scrollHeightEnd = dim.getHeight() * 0.2;
            int scrollEnd = scrollHeightEnd.intValue(); //478

            TouchAction action = new TouchAction(Driver);

            action.press(point(10, scrollStart));//(10, 478)
            action.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(5)));
            action.moveTo(point(600, scrollEnd)); //(600, 478)
            action.release();
            action.perform();

            System.out.println("Drew a straight line --------------- ");
            Assert.assertTrue("Drew a straight line --------------- ", true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to draw a straight long line  ---  " + e.getMessage());
        }
    }

    public static void drawStraightLongLine(int fromPosition, int toPosition, String errorMessage) {

        try {
            //Getting screen size
            Dimension dim = Driver.manage().window().getSize();

            Double scrollHeightStart = dim.getHeight() * 0.5; //1196.0
            int scrollStart = scrollHeightStart.intValue(); //1196

            Double scrollHeightEnd = dim.getHeight() * 0.2;
            int scrollEnd = scrollHeightEnd.intValue(); //478

            TouchAction action = new TouchAction(Driver);

            action.press(point(fromPosition, scrollStart));//(10, 478)
            action.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(5)));
            action.moveTo(point(toPosition, scrollEnd)); //(600, 478)
            action.release();
            action.perform();

            System.out.println("Drew a straight line --------------- ");
            Assert.assertTrue("Drew a straight line --------------- ", true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to draw a straight long line  ---  " + e.getMessage());
        }
    }
    //endregion

    //region <enterText>
    public static void enterText(By element, String textToEnter, String errorMessage) {
        try {
            if (textToEnter.equals("")) {
                System.out.println("There is No text to enter");
                Assert.assertTrue("There is No text to enter", true);
            }
            else {
                waitForElement(element, errorMessage);
                WebElement elementToTypeIn = Driver.findElement(element);
                elementToTypeIn.click();
                elementToTypeIn.clear();
                elementToTypeIn.click();
                elementToTypeIn.sendKeys(textToEnter);

                System.out.println("Entered text: \"" + textToEnter + "\" to: " + element);
                Assert.assertTrue("Entered text: \"" + textToEnter + "\" to: " + element, true);
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to enter text: \"" + textToEnter + "\" ---  " + element + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <enterTextAndPress_Enter>
    /*This is method is used to enter a test then press key "Enter"*/
    public static void enterTextAndPress_Enter(By element, String textToEnter, String errorMessage) {

        try {
            if (textToEnter.equals("")) {
                System.out.println("There is No text to enter");
                Assert.assertTrue("There is No text to enter", true);
            }
            else {
                waitForElement(element, errorMessage);
                WebElement elementToTypeIn = Driver.findElement(element);
                elementToTypeIn.click();
                elementToTypeIn.clear();
                elementToTypeIn.click();
                elementToTypeIn.sendKeys(textToEnter + "\n");

                System.out.println("Entered text: \"" + textToEnter + "\" to: " + element + " and press Enter");
                Assert.assertTrue("Entered text: \"" + textToEnter + "\" to: " + element + " and press Enter", true);
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to enter text \"" + textToEnter + "\" and press Enter in element --- " + element + " --- " + e.getMessage());
        }
    }
    //endregion

    //region <enterTextAndPressEnter>
    /*This is method is used to enter a test then press key "Enter"*/
    public static void enterTextAndPressEnter(By element, String textToEnter, String errorMessage) {

        try {
            AndroidDriver androidDriver = Driver;

            if (textToEnter.equals("")) {
                System.out.println("There is No text to enter");
                Assert.assertTrue("There is No text to enter", true);
            }
            else {
                waitForElement(element, errorMessage);
                WebElement elementToTypeIn = Driver.findElement(element);
                elementToTypeIn.click();
                elementToTypeIn.clear();
                elementToTypeIn.click();
                elementToTypeIn.sendKeys(textToEnter);
                androidDriver.pressKey(new KeyEvent(AndroidKey.ENTER)); //KEYCODE_ENTER

                System.out.println("Entered text: \"" + textToEnter + "\" to: " + element + " and press Enter");
                Assert.assertTrue("Entered text: \"" + textToEnter + "\" to: " + element + " and press Enter", true);
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to enter text \"" + textToEnter + "\" and press Enter in element --- " + element + " --- " + e.getMessage());
        }
    }
    //endregion

    //region <enterTextAndPressReturn>
    /*This is method is used to enter a test then press key "Return"*/
    public static void enterTextAndPressReturn(By element, String textToEnter, String errorMessage) {

        try {
            if (textToEnter.equals("")) {
                System.out.println("There is No text to enter");
                Assert.assertTrue("There is No text to enter", true);
            }
            else {
                waitForElement(element, errorMessage);
                WebElement elementToTypeIn = Driver.findElement(element);
                elementToTypeIn.click();
                elementToTypeIn.clear();
                elementToTypeIn.click();
                elementToTypeIn.sendKeys(textToEnter);
                elementToTypeIn.sendKeys(Keys.RETURN);

                System.out.println("Entered text : \"" + textToEnter + "\" to : " + element + " and press Return");
                Assert.assertTrue("Entered text : \"" + textToEnter + "\" to : " + element + " and press Return", true);
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to enter text \"" + textToEnter + "\" and press Return in element - " + element + " --- " + e.getMessage());
        }
    }
    //endregion

    //region <enterTextByIdWithJavaScript>
    public static void enterTextByIdWithJavaScript(String elementId, String textToEnter, String errorMessage) {

        try {
            JavascriptExecutor jse = Driver;
            //Thread.sleep(500);
            jse.executeScript("document.getElementById('" + elementId + "').value = '" + textToEnter + "';");
        }
        catch(Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to enter text : \"" + textToEnter + "\" ---  " + elementId + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <enterTextWithoutClearing>
    public static void enterTextWithoutClearing(By element, String textToEnter, String errorMessage) {

        try {
            if (textToEnter.equals("")) {
                Assert.assertTrue("There is No text to enter", true);
            }
            else {
                waitForElement(element, errorMessage);
                WebElement elementToTypeIn = Driver.findElement(element);
                elementToTypeIn.click();
                elementToTypeIn.sendKeys(textToEnter);

                System.out.println("Entered text: \"" + textToEnter + "\" to : " + element);
                Assert.assertTrue("Entered text: \"" + textToEnter + "\" to : " + element, true);
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to enter text: \"" + textToEnter + "\"  without clearing ---  " + element + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <enterTextWithAdb>
    /**
     * Enter text using "adb" which is faster than using senKeys
     */
    public static void enterTextWithAdb(By element, String textToEnter, String uuid, String errorMessage) {

        try {
            waitForElement(element, errorMessage);
            WebElement elementToTypeIn = Driver.findElement(element);
            elementToTypeIn.click();
            new ProcessBuilder("adb", "-s", uuid, "shell", "input", "text", textToEnter)
                    .redirectErrorStream(true)
                    .start();

            System.out.println("Entered text: \"" + textToEnter + "\" to : " + element);
            Assert.assertTrue("Entered text: \"" + textToEnter + "\" to : " + element, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to enter text with adb: \"" + textToEnter + "\"  without clearing ---  " + element + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <enterTextWithAdbAndPressEnter>
    /**
     * Enter text using "adb" which is faster than using senKeys
     * then press ENTER key.
     * You need to pass the "uuid" of the emulator
     */
    public static void enterTextWithAdbAndPressEnter(By element, String textToEnter, String uuid, String errorMessage) {

        try {
            AndroidDriver androidDriver = Driver;
            waitForElement(element, errorMessage);
            WebElement elementToTypeIn = Driver.findElement(element);
            elementToTypeIn.click();
            new ProcessBuilder("adb", "-s", uuid, "shell", "input", "text", textToEnter)
                    .redirectErrorStream(true)
                    .start();
            androidDriver.pressKey(new KeyEvent(AndroidKey.ENTER)); //KEYCODE_ENTER

            System.out.println("Entered text : \"" + textToEnter + "\" to : " + element);
            Assert.assertTrue("Entered text : \"" + textToEnter + "\" to : " + element, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to enter text : \"" + textToEnter + "\"  without clearing ---  " + element + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <enterTextWithAdbAndPress_Enter>
    /**
     * Enter text using "adb" which is faster than using senKeys
     * then press ENTER key.
     * You need to pass the "uuid" of the emulator
     */
    public static void enterTextWithAdbAndPress_Enter(By element, String textToEnter, String uuid, String errorMessage) {

        try {
            AndroidDriver androidDriver = Driver;
            waitForElement(element, errorMessage);
            WebElement elementToTypeIn = Driver.findElement(element);
            elementToTypeIn.click();
            new ProcessBuilder("adb", "-s", uuid, "shell", "input", "text", textToEnter)
                    .redirectErrorStream(true)
                    .start();
            pressAndroidKey("ENTER"); //KEYCODE_ENTER

            log("Entered text : \"" + textToEnter + "\" to : " + element, "ERROR",  "text");
            Assert.assertTrue("Entered text : \"" + textToEnter + "\" to : " + element, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to enter text : \"" + textToEnter + "\"  without clearing ---  " + element + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <enterTextWithSetValue>
    /**
     * Clear Textbox then
     * Enter text using "setValue" which is faster than using senKeys
     */
    public static void enterTextWithSetValue(By element, String textToEnter, String errorMessage) {

        try {
            waitForElement(element, errorMessage);
            MobileElement elementToTypeIn = (MobileElement) Driver.findElement(element);
            elementToTypeIn.click();
            elementToTypeIn.clear();
            elementToTypeIn.click();
            elementToTypeIn.setValue(textToEnter);

            System.out.println("Entered text: \"" + textToEnter + "\" to : " + element);
            Assert.assertTrue("Entered text: \"" + textToEnter + "\" to : " + element, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to enter text with setValue: \"" + textToEnter + "\"  without clearing ---  " + element + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <enterTextWithSetValueWithoutClearing>
    /**
     * Enter text using "setValue" which is faster than using senKeys
     */
    public static void enterTextWithSetValueWithoutClearing(By element, String textToEnter, String errorMessage) {

        try {
            waitForElement(element, errorMessage);
            MobileElement elementToTypeIn = (MobileElement) Driver.findElement(element);
            elementToTypeIn.click();
            elementToTypeIn.setValue(textToEnter);

            System.out.println("Entered text: \"" + textToEnter + "\" to : " + element);
            Assert.assertTrue("Entered text: \"" + textToEnter + "\" to : " + element, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to enter text with setValue: \"" + textToEnter + "\"  without clearing ---  " + element + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <extractText>
    public static String extractText(By element, String errorMessage) {

        String retrievedText = "";
        try {
            waitForElement(element, errorMessage);
            WebElement elementToRead = Driver.findElement(element);
            retrievedText = elementToRead.getText();

            log("Extracted text: '" + retrievedText + "' retrieved from element --- " + element, "ERROR",  "text");
            Assert.assertTrue("Extracted text: " + retrievedText + " retrieved from element --- " + element, true);

            return retrievedText;
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to retrieve text from element --- " + element + " - " + e.getMessage());

            return retrievedText;
        }
    }
    //endregion

    //region <extractTextBetweenSpecialCharWithRegex>
    /**
     * To extract string between "(" and ")" using Regular Expression
     */
    public static String extractTextBetweenSpecialCharWithRegex(String string, String errorMessage) {

        String subString = "";

        try {
            subString = string.split("[()]")[1];
            System.out.println("Extracted substring \"" + subString + "\" from string \"" + string + "\"");
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to extract substring \"" + subString + "\" from string \"" + string + "\" --- " + e.getMessage());
        }

        return subString;
    }
    //endregion

    //region <extractTextBeforeSpaceWithRegex>
    /**
     * To remove anything after a space " " using Regular Expression
     */
    public static String extractTextBeforeSpaceWithRegex(String string, String errorMessage) {

        String subString = "";

        try {
            subString = string.replaceAll(" .+$", "");
            System.out.println("Extracted substring \"" + subString + "\" from string \"" + string + "\"");
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to extract substring \"" + subString + "\" from string \"" + string + "\" --- " + e.getMessage());
        }

        return subString;
    }
    //endregion

    //region <getCurrentTimeUsingCalendar>
    public static void getCurrentTimeUsingCalendar() {

        try {
            Calendar cal = Calendar.getInstance();
            Date date = cal.getTime();
            DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
            String formattedDate = dateFormat.format(date);
            log("Current time of the day using Calendar - 24 hour format: " + formattedDate, "INFO", "text");
        }
        catch (Exception e) {
            log("Failed to get the current time", "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to get the current time --- " + e.getMessage());
        }
    }
    //endregion

    //region <hoverOverElement>
    public static void hoverOverElementAndClickSubElementByXpath(String elementXpath, String subElementToClickXpath, String errorMessage) {

        try {
            waitForElementByXpath(elementXpath, errorMessage);

            Actions hoverTo = new Actions(Driver);
            hoverTo.moveToElement(Driver.findElement(By.xpath(elementXpath)));
            hoverTo.perform();
            pause(1000);
            hoverTo.moveToElement(Driver.findElement(By.xpath(subElementToClickXpath)));
            hoverTo.click();
            hoverTo.perform();

            System.out.println("Hovered over element - " + elementXpath + " and clicked sub element " + subElementToClickXpath);
            Assert.assertTrue("Hovered over element - " + elementXpath + " and clicked sub element " + subElementToClickXpath, true);
        }
        catch (Exception e) {
            System.out.println("Failed to hover over element and click sub element by Xpath  - " + e.getMessage());
            Assert.fail("\n[ERROR] Failed to hover over element Xpath : " + elementXpath + " and click sub element " + subElementToClickXpath + " ---  " + e.getMessage());
        }
    }

    public static void hoverOverElementAndClickSubElement(By element, By subElementToClick, String errorMessage) {

        try {
            waitForElement(element, errorMessage);

            Actions hoverTo = new Actions(Driver);
            hoverTo.moveToElement(Driver.findElement(element));
            hoverTo.perform();
            pause(1000);
            hoverTo.moveToElement(Driver.findElement(subElementToClick));
            hoverTo.click();
            hoverTo.perform();

            System.out.println("Hovered over element - " + element + " and clicked sub element " + subElementToClick);
            Assert.assertTrue("Hovered over element - " + element + " and clicked sub element " + subElementToClick, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to hover over element : " + element + " and click sub element " + subElementToClick + " ---  " + e.getMessage());
        }
    }

    public static void hoverOverElement(By element, String errorMessage) {

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(element));
            wait.until(ExpectedConditions.elementToBeClickable(element));
            WebElement ele = Driver.findElement(element);
            Actions actions = new Actions(Driver);
            actions.moveToElement(ele).build().perform();

            System.out.println("Hovered over element - " + element);
            Assert.assertTrue("Hovered over element - " + element, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("[ERROR] Failed to hover over element : " + element + " ---  " + e.getMessage());
        }
    }

    public static void hoverOverElementByXpath(String elementXpath, String errorMessage) {

        try {
            waitForElementByXpath(elementXpath, errorMessage);

            Actions hoverTo = new Actions(Driver);
            hoverTo.moveToElement(Driver.findElement(By.xpath(elementXpath)));
            hoverTo.perform();

            System.out.println("Hovered over element - " + elementXpath);
            Assert.assertTrue("Hovered over element - " + elementXpath, true);
        }
        catch (Exception e) {
            System.out.println("Failed to hover over element Xpath - " + e.getMessage());
            Assert.fail("\n[ERROR] Failed to hover over element Xpath : " + elementXpath + " ---  " + e.getMessage());
        }
    }
    //endregion

    //region <installApp>
    public static void installApp(String appPath) {

        try {
            Driver.installApp(appPath);

            System.out.println("Installed App successfully");
            Assert.assertTrue("Installed App successfully", true);
        }
        catch (Exception e) {
            log("Failed to install App: \"" + appPath + "\"", "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to install App: \"" + appPath + "\" --- " + e.getMessage());
        }
    }
    //endregion

    //region <isFocused>
    /**
     * This method is used to check if element's attribute is Focused
     */
    public static void isFocused(By element) {

        try {
            WebElement webText = wait.until(ExpectedConditions.presenceOfElementLocated(element));
            String str = webText.getAttribute("focused");

            if (str.equals("true")) {
                log("Element is Focused -- " + element, "INFO", "text");
            } else {
                Assert.fail("\nElement is NOT Focused -- " + element);
            }
        } catch (Exception e) {
            log("Cursor is not focused on element", "ERROR", "text");
            Assert.fail("\n[ERROR] Cursor is not focused on element: " + element + " --- " + e.getMessage());
        }
    }
    //endregion

    //region <getElementAttribute>
    /**
     * This method is used to extract element's attribute
     */
    public static String getElementAttribute(By element, String attribute) {

        WebElement webText = wait.until(ExpectedConditions.presenceOfElementLocated(element));
        return webText.getAttribute(attribute);
    }
    //endregion

    //region <launchApp>
    /**
     * Launch App running in the Background
     * to come back to Forefront
     */
    public static void launchApp() {

        try {
            Driver.launchApp();

            System.out.println("Launch App successfully");
            Assert.assertTrue("Launch App successfully", true);
        }
        catch (Exception e) {
            log("Failed to Launch App", "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to Launch App --- " + e.getMessage());
        }
    }
    //endregion

    //region <longPress>
    public static void longPress(By element, String errorMessage) {

        try {
            TouchActions action = new TouchActions(Driver);
            action.longPress((WebElement) element);
            action.perform();

            System.out.println("Long pressed element: " + element);
            Assert.assertTrue("Long pressed element: " + element, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to long press element ---  " + element + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <longPressAndHold>
    public static void longPressAndHold(By element, String errorMessage) {

        try {
            TouchActions action = new TouchActions(Driver);
            action.longPress((WebElement) element).release();
            action.perform();

            System.out.println("Long pressed & hold element: " + element);
            Assert.assertTrue("Long pressed element: " + element, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to long press & hold element ---  " + element + "' - " + e.getMessage());
        }
    }
    //endregion

    //region <longPressDragAndDrop>
    public static void longPressDragAndDrop(By elementStart, By elementEnd, String errorMessage) {

        try {

            WebElement startPoint = Driver.findElement(elementStart);
            WebElement endPoint = Driver.findElement(elementEnd);

            TouchAction dragNDrop = new TouchAction(Driver)
                    .longPress(longPressOptions()
                            .withElement(element(startPoint))
                            .withDuration(ofSeconds((long) 0.5)))
                    .moveTo(element(endPoint))
                    .release();
            dragNDrop.perform();

            System.out.println("Long pressed from: " + startPoint + " to: " + endPoint);
            Assert.assertTrue("Long pressed from: " + startPoint + " to: " + endPoint, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to long press element ---  " + elementStart + " and dragged & dropped - " + e.getMessage());
        }
    }
    //endregion

    //region <longPressDragAndDropByCoordinate>
    public static void longPressDragAndDropByCoordinate(By element, By elementEnd, String errorMessage) {

        try {
            WebElement startPoint = Driver.findElement(By.id("io.appium.android.apis:id/drag_dot_1"));
            WebElement endPoint = Driver.findElement(By.id("io.appium.android.apis:id/drag_dot_3"));

            TouchAction dragNDrop = new TouchAction(Driver)
                    .longPress(longPressOptions()
                            .withElement(element(startPoint))
                            .withDuration(ofSeconds((long) 0.5)))
                    .moveTo(element(endPoint))
                    .release();
            dragNDrop.perform();

            System.out.println("Long pressed from: " + startPoint + " to: " + endPoint);
            Assert.assertTrue("Long pressed from: " + startPoint + " to: " + endPoint, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to long press element ---  " + element + " and dragged & dropped - " + e.getMessage());
        }
    }
    //endregion

    //region <pause>
    public static void pause(int millisecondsWait) {

        try {
            Thread.sleep(millisecondsWait);
        }
        catch (Exception e) {
            log("Failed to pause", "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to pause --- " + e.getMessage());
        }
    }
    //endregion

    //region <pullToRefresh>
    public static void pullToRefresh() {

        try {
            //Get the size of screen.
            Dimension size = Driver.manage().window().getSize();
            int endX = 0;
            //Find swipe start and end point from screen’s width and height.
            //Find startY point which is at bottom side of screen.
            int startY = (int) (size.height * 0.90);
            //Find endY point which is at top side of screen.
            int endY = (int) (size.height * 0.20);
            //Find horizontal point where you wants to swipe. It is in middle of screen width.
            int startX = size.width / 2;
            //Swipe from Top to Bottom
            TouchAction action = new TouchAction(Driver);
            action.press(PointOption.point(startX, startY));
            action.waitAction();
            action.moveTo(PointOption.point(endX, startY));
            action.release();
            action.perform();

            System.out.println("Pulled and refreshed page successfully");
            Assert.assertTrue("Pulled and refreshed page successfully", true);
        }
        catch (Exception e) {
            log("Failed to refresh page", "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to refresh page --- " + e.getMessage());
        }
    }
    //endregion

    //region <pressAndHold>
    public static void pressAndHold(By element,int timeout, String errorMessage) {

        try {
            TouchAction action = new TouchAction(Driver);
            action.longPress(longPressOptions().withElement(element((WebElement) element))
                    .withDuration(Duration.ofMillis(timeout)))
                    .release()
                    .perform();

            System.out.println("Long pressed & Hold element: " + element);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to press and hold --- " + e.getMessage());
        }
    }
    //endregion

    //region <pressAndHoldReturn>
    /**
     * This method is used to press & hold MDX value on RET tab
     * from Branch App
     */
    public static void pressAndHoldReturn(String text, String errorMessage) {

        try {
            String mdxXpath = "//android.widget.TextView[@text='" + text + " (0)']";
            WebElement elmt = Driver.findElement(By.xpath(mdxXpath));

            TouchAction action = new TouchAction(Driver);

            action.longPress(longPressOptions().withElement(element(elmt))
                    .withDuration(Duration.ofMillis(5000)))
                    .release()
                    .perform();

            System.out.println("Long pressed & Hold element: " + text);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to press and hold --- " + e.getMessage());
        }
    }
    //endregion

    //region <pressAndroidKey>
    /**
     * To get more code go to this link
     * https://stackoverflow.com/questions/11768356/need-table-of-key-codes-for-android-and-presenter
     * https://developer.android.com/reference/android/view/KeyEvent
     */
    public static void pressAndroidKey(String keyToPress, String errorMessage) {

        try {
            AndroidDriver androidDriver = Driver;

            switch (keyToPress.toUpperCase()) {
                case "ARROW UP": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.DPAD_UP));
                }
                case "ARROW DOWN": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.DPAD_DOWN));
                }
                case "TAB": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.TAB));
                }
                case "SPACE": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.SPACE));
                }
                case "ENTER": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.ENTER));
                }
                case "DELETE": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.DEL));
                }
                case "SEARCH": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.SEARCH));
                }
                case "HOME": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.HOME));
                }
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to press Key \"" + keyToPress + "\" -  " + e.getMessage());
        }
    }

    public static void pressAndroidKey(String keyToPress) {

        try {
            AndroidDriver androidDriver = Driver;

            switch (keyToPress.toUpperCase()) {
                case "ARROW UP": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.DPAD_UP));
                }
                case "ARROW DOWN": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.DPAD_DOWN));
                }
                case "TAB": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.TAB));
                }
                case "SPACE": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.SPACE));
                }
                case "ENTER": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.ENTER));
                }
                case "DELETE": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.DEL));
                }
                case "SEARCH": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.SEARCH));
                }
                case "HOME": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.HOME));
                }
            }
        }
        catch (Exception e) {
            log("Failed to press Key \"" + keyToPress + "\"", "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to press Key \"" + keyToPress + "\" -  " + e.getMessage());
        }
    }
    //endregion

    //region <pressKey>
    public static void pressKey(String keyToPress, String errorMessage) {

        try {

            AndroidDriver androidDriver = Driver;

            switch (keyToPress.toUpperCase()) {
                case "ARROW UP": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.DPAD_UP));
                }
                case "ARROW DOWN": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.DPAD_DOWN));
                }
                case "TAB": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.TAB));
                }
                case "SPACE": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.SPACE));
                }
                case "ENTER": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.ENTER));
                }
                case "DELETE": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.DEL));
                }
                case "SEARCH": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.SEARCH));
                }
                case "HOME": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.HOME));
                }
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to press Key \"" + keyToPress + "\" -  " + e.getMessage());
        }
    }
    //endregion

    //region <refresh>
    /**
     * Refresh page by swiping the screen down and releasing
     */
    public static void refreshPage() {

        try {
            Dimension size = Driver.manage().window().getSize();

            int startX;
            int endX = 0;
            int startY;

            startY = (int) (size.height * 0.70);
            startX = (size.width / 2);

            TouchAction action = new TouchAction(Driver);
            action.press(PointOption.point(startX, startY));
            action.waitAction();
            action.moveTo(PointOption.point(endX, startY));
            action.release();
            action.perform();

            System.out.println("Refreshed page successfully");
            Assert.assertTrue("Refreshed page successfully", true);
        }
        catch (Exception e) {
            log("Failed to refresh page", "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to refresh page --- " + e.getMessage());
        }
    }
    //endregion

    //region <runAppInBackground>
    /**
     * Run App in the background for X(timeout) seconds
     * without closing it and comes back automatically
     */
    public static void runAppInBackground(int timeout) {

        try {
            Driver.runAppInBackground(Duration.ofSeconds(timeout));

            System.out.println("Ran App in background successfully");
            Assert.assertTrue("Ran App In background successfully", true);
        }
        catch (Exception e) {
            log("Failed to run App in background", "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to run App in background --- " + e.getMessage());
        }
    }

    /**
     * Run App in the background without closing it or
     * coming back automatically
     */
    public static void runAppInBackground() {

        try {
            Driver.runAppInBackground(Duration.ofSeconds(-1));

            System.out.println("Ran App in background successfully");
            Assert.assertTrue("Ran App In background successfully", true);
        }
        catch (Exception e) {
            log("Failed to run App in background", "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to run App in background --- " + e.getMessage());
        }
    }
    //endregion

    //region <removeApp>
    public static void removeApp(String appPath) {

        try {
            Driver.removeApp(appPath);

            System.out.println("Removed App successfully");
            Assert.assertTrue("Removed App successfully", true);
        }
        catch (Exception e) {
            log("Failed to remove App: \"" + appPath + "\"", "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to remove App: \"" + appPath + "\" --- " + e.getMessage());
        }
    }
    //endregion

    //region <scrollDown>
    public static void scrollDown(String errorMessage) {

        try {
            //Getting screen size
            Dimension dim = Driver.manage().window().getSize();

            Double scrollHeightStart = dim.getHeight() * 0.5;
            int scrollStart = scrollHeightStart.intValue();

            Double scrollHeightEnd = dim.getHeight() * 0.2;
            int scrollEnd = scrollHeightEnd.intValue();

            TouchAction action = new TouchAction(Driver);

            action.press(point(0, scrollStart));
            action.waitAction(WaitOptions.waitOptions(Duration.ofSeconds(2)));
            action.moveTo(point(0, scrollEnd));
            action.release();
            action.perform();
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to scroll down --- " + e.getMessage());
        }
    }
    //endregion

    //region <scrollDown>
    public static void scrollDown() {

        try {
            JavascriptExecutor jse = Driver;
            jse.executeScript("window.scrollBy(0,250)", "");
            System.out.println("Scrolled Down");
            Assert.assertTrue("Scrolled Down", true);
        }
        catch (Exception e) {
            log("Failed to scroll Down", "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to scroll Down - " + e.getMessage());
        }
    }
    //endregion

    //region <scrollDown>
    public static void scrollDown(String elementXpath, String errorMessage) {

        try {
            JavascriptExecutor js = Driver;
            System.out.println("Swiping to element " + elementXpath);
            js.executeScript("mobile: scrollTo", elementXpath);
            Driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
            Assert.assertTrue("Scrolled Down", true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to scroll Down - " + e.getMessage());
        }
    }
    //endregion

    //region <scrollDown>
    public static void scrollDownToElementByXpath(String elementXpath, String valueToSelect, String errorMessage) {

        String updatedXpath = "";

        try {
            updatedXpath = updateXpathValueWithString(elementXpath, valueToSelect); //update default value saved in .properties

            JavascriptExecutor jse = Driver;
            jse.executeScript("window.scrollBy(0,250)", "");
            System.out.println("Scrolled Down");
            waitForElementByXpath(updatedXpath, errorMessage);
            Assert.assertTrue("Scrolled Down", true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to scroll down to Element - "  + updatedXpath + " - " + e.getMessage());
        }
    }
    //endregion

    //region <scrollDown>
    public static void scrollDownToElement(String elementXpath, String valueToSelect, String errorMessage) {

        String updatedXpath = "";

        try {
            updatedXpath = updateXpathValueWithString(elementXpath, valueToSelect); //update default value saved in .properties

            JavascriptExecutor jse = Driver;
            jse.executeScript("mobile: scrollTo", updatedXpath);
            System.out.println("Scrolled Down");
            waitForElementByXpath(updatedXpath, errorMessage);
            Assert.assertTrue("Scrolled Down", true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to scroll down to Element - "  + updatedXpath + " - " + e.getMessage());
        }
    }
    //endregion

    //region <scrollUp>
    public static void scrollUp() {

        try {
            JavascriptExecutor jse = Driver;
            jse.executeScript("window.scrollBy(0,-250)", "");
            System.out.println("Scrolled Up");
            Assert.assertTrue("Scrolled Up", true);
        }
        catch (Exception e) {
            System.out.println("Failed to scroll Up - " + e.getMessage());
            Assert.fail("\n[ERROR] Failed to scroll Up - " + e.getMessage());
        }
    }
    //endregion

    //region <scrollDownToBottom>
    public static void scrollDownToBottom(String errorMessage) {

        try {
            //Getting screen size
            Dimension dim = Driver.manage().window().getSize();

            int height = dim.getHeight();
            int width = dim.getWidth();
            int x = width / 2;

            int top_y = (int) (height * 0.80);
            int bottom_y = (int) (height * 0.20);

            TouchAction action = new TouchAction(Driver);

            action.press(PointOption.point(x, top_y)); //Scroll from top of screen no matter the size of device
            action.waitAction();
            action.waitAction();
            action.moveTo(PointOption.point(x, bottom_y)); //Scroll to the bottom of the screen no mater the size of the device
            action.release();
            action.perform();
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to scroll to bottom --- " + e.getMessage());
        }
    }
    //endregion

    //region <scrollToBranchSlowlyAndClick>
    /**
     * Working code to scroll slowly to an element then click it,
     * based on set up ratio of screen
     * */
    public static void scrollToBranchSlowlyAndClick(By element, String valueToSelect, String errorMessage) {

        try {
            List<WebElement> elmList;

            int count = 0;
            elmList = Driver.findElements(element);
            boolean found =  false;

            //Getting screen size
            Dimension dim = Driver.manage().window().getSize(); //dim = (480, 800) Driver palm

            int height = dim.getHeight(); //height = 800
            int width = dim.getWidth(); //width = 480
            int x = width / 2; //x = 240

            int top_y = (int) (height * 0.80); //top = 640
            int bottom_y = (int) (height * 0.20); //top = 160 == > 800

            while(count < bottom_y && !found) {

                for(int j = 0; j < elmList.size(); j++) {
                    if(elmList.get(j).getText().equalsIgnoreCase(valueToSelect)) {
                        elmList.get(j).click();
                        found = true;
                        break;
                    }
                }

                if(!found) {
                    TouchAction action = new TouchAction(Driver);

                    action.press(PointOption.point(x, 845)); //Scroll from top of screen
                    action.waitAction();
                    action.moveTo(PointOption.point(x, 650)); //Scroll to the bottom of the screen slowly
                    action.release();
                    action.perform();

                    elmList = Driver.findElements(element);
                    count++;
                }
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to scroll slowly to branch - "  + valueToSelect + " --- " + e.getMessage());
        }
    }
    //endregion

    //region <scrollToElement>
    public static void scrollToElement(By element, String errorMessage) {

        try {
            waitForElement(element, errorMessage);
            WebElement elmt = Driver.findElement(element);
            ((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", elmt);
            Thread.sleep(500);

            System.out.println("Scrolled To Element - " + element);
            Assert.assertTrue("Scrolled To Element - " + element, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to scroll To Element - "  + element + " --- " + e.getMessage());
        }
    }
    //endregion

    //region <scrollToElement_>
    public static void scrollToElement_(By element, String valueToSelect, String errorMessage) {

        try {
            List<WebElement> elmList;

            int count = 0;
            elmList = Driver.findElements(element);

            while (count < 5) {
                for (int i = 0; i < elmList.size(); i++) {

                    if (elmList.get(i).getText().equalsIgnoreCase(valueToSelect)) {
                        elmList.get(i).click();
                        break;
                    }
                    else {
                        TouchAction action = new TouchAction(Driver);

                        action.press(PointOption.point(550, 1500)); //(x, y) coordinate for the starting point of scrolling
                        action.waitAction();
                        action.moveTo(PointOption.point(550, 1300)); //(x, y) coordinate for the ending point of scrolling
                        action.release();
                        action.perform();
                    }
                }

                count++;
                elmList = Driver.findElements(element);
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to scroll to element \"" + valueToSelect + "\" and select --- " + e.getMessage());
        }
    }
    //endregion

    //region <scrollToElementAndClick>
    /**
     * Working code to scroll to an element then click it,
     * based on set up ratio of screen
     * */
    public static void scrollToElementAndClick(By element, String valueToSelect, String errorMessage) {

        try {
            List<WebElement> elmList;

            int count = 0;
            elmList = Driver.findElements(element);
            boolean found =  false;

            //Getting screen size
            Dimension dim = Driver.manage().window().getSize();

            int height = dim.getHeight();
            int width = dim.getWidth();
            int x = width / 2;

            int top_y = (int) (height * 0.80);
            int bottom_y = (int) (height * 0.20);

            while (count < bottom_y && !found) {

                for (int j = 0; j < elmList.size(); j++) {
                    if (elmList.get(j).getText().equalsIgnoreCase(valueToSelect)) {
                        elmList.get(j).isDisplayed();
                        elmList.get(j).click();
                        found = true;
                        break;
                    }
                }

                if(!found) {
                    TouchAction action = new TouchAction(Driver);

                    action.press(PointOption.point(x, top_y)); //Scroll from top of screen no matter the size of device
                    action.waitAction();
                    action.moveTo(PointOption.point(x, bottom_y)); //Scroll to the bottom of the screen no mater the size of the device
                    action.release();
                    action.perform();

                    elmList = Driver.findElements(element);
                    count++;
                }
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to scroll to element: " + element + " and click - "  + valueToSelect + " --- " + e.getMessage());
        }
    }

    public static void scrollToElementToClick(By element, String valueToSelect, String errorMessage) {

        try {
            List<WebElement> elmList;

            int count = 0;
            elmList = Driver.findElements(element);
            boolean found =  false;

            //Getting screen size
            Dimension dim = Driver.manage().window().getSize();

            int height = dim.getHeight();
            int width = dim.getWidth();
            int x = width / 2;

            int top_y = (int) (height * 0.80);
            int bottom_y = (int) (height * 0.20);

            while (count < bottom_y && !found) {
                for (int j = 0; j < elmList.size(); j++) {
                    if (elmList.get(j).getText().equalsIgnoreCase(valueToSelect)) {
                        waitForElementToBeDisplayed(elmList.get(j), 5); //wait for value to be clicked to be displayed
                        elmList.get(j).click();
                        found = true;
                        break;
                    }
                }

                if(!found) {
                    TouchAction action = new TouchAction(Driver);

                    action.press(PointOption.point(x, top_y)); //Scroll from top of screen no matter the size of device
                    action.waitAction();
                    action.moveTo(PointOption.point(x, bottom_y)); //Scroll to the bottom of the screen no mater the size of the device
                    action.release();
                    action.perform();

                    elmList = Driver.findElements(element);
                    count++;
                }
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to scroll to element: " + element + " and click - "  + valueToSelect + " --- " + e.getMessage());
        }
    }
    //endregion

    //region <scrollToElementSlowlyAndClick>
    /**
     * Working code to scroll slowly to an element then click it,
     * based on set up ratio of screen
     * */
    public static void scrollToElementSlowlyAndClick(By element, String valueToSelect, String errorMessage) {

        try {
            List<WebElement> elmList;

            int count = 0;
            elmList = Driver.findElements(element);
            boolean found =  false;

            //Getting screen size
            Dimension dim = Driver.manage().window().getSize(); //dim = (480, 800) Driver palm

            int height = dim.getHeight(); //height = 800
            int width = dim.getWidth(); //width = 480
            int x = width / 2; //x = 240

            int top_y = (int) (height * 0.80); //top = 640
            int bottom_y = (int) (height * 0.20); //top = 160 == > 800

            while (count < bottom_y && !found) {

                for (int j = 0; j < elmList.size(); j++) {
                    if (elmList.get(j).getText().equalsIgnoreCase(valueToSelect)) {
                        elmList.get(j).click();
                        found = true;
                        break;
                    }
                }

                if(!found) {
                    TouchAction action = new TouchAction(Driver);

                    action.press(PointOption.point(x, 655)); //Scroll from top of screen no matter the size of device
                    action.waitAction();
                    action.moveTo(PointOption.point(x, 450)); //Scroll to the bottom of the screen slowly
                    action.release();
                    action.perform();

                    elmList = Driver.findElements(element);
                    count++;
                }
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to scroll to element: " + element + " and click - "  + valueToSelect + " --- " + e.getMessage());
        }
    }
    //endregion

    //region <scrollTo_ElementAndClick>
    /**
     * Working code to scroll to an element then click it,
     * based on set up ratio of screen
     * */
    public static void scrollTo_ElementAndClick(By element, String valueToSelect, String errorMessage) {

        try {
            List<WebElement> elmList;

            int count = 0;
            elmList = Driver.findElements(element);
            boolean found =  false;

            //Getting screen size
            Dimension dim = Driver.manage().window().getSize();

            int height = dim.getHeight();
            int width = dim.getWidth();
            int x = width / 2;

            int top_y = (int) (height * 0.80);
            int bottom_y = (int) (height * 0.20);

            while (count < bottom_y && !found) {

                for (int j = 0; j < elmList.size(); j++) {
                    System.out.println("List: " + elmList.get(j).getText());
                    if (elmList.get(j).getText().equalsIgnoreCase(valueToSelect)) {
                        System.out.println("Found: " + elmList.get(j).getText());
                        elmList.get(j).click();
                        found = true;
                        break;
                    }
                }

                if(!found) {
                    TouchAction action = new TouchAction(Driver);

                    action.press(PointOption.point(x, top_y)); //Scroll from top of screen no matter the size of device
                    action.waitAction();
                    action.moveTo(PointOption.point(x, bottom_y)); //Scroll to the bottom of the screen no mater the size of the device
                    action.release();
                    action.perform();

                    elmList = Driver.findElements(element);

                    count++;
                }
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to scroll to Element - "  + valueToSelect + " - " + e.getMessage());
        }
    }
    //endregion

    //region <scrollToElementAndSelect>
    public static void scrollToElementAndSelect(String elementXpath, String valueToSelect, String errorMessage) {

        String updatedXpath = "";

//        WebElement element = null;
        AndroidElement element = null;
        int numberOfTimes = 10;
        Dimension size = Driver.manage().window().getSize();
        int anchor = size.width / 2;
        // Swipe up to scroll down
        int startPoint = size.height - 10;
        int endPoint = 10;

        for (int i = 0; i < numberOfTimes; i++) {
            try {
                new TouchAction(Driver)
                        .longPress(point(anchor, startPoint))
                        .moveTo(point(anchor, endPoint))
                        .release()
                        .perform();
                updatedXpath = updateXpathValueWithString(elementXpath, valueToSelect); //update default value saved in .properties
//                element = Driver.findElement(By.xpath(updatedXpath));
                element = (AndroidElement) Driver.findElement(By.xpath(updatedXpath));
                i = numberOfTimes;
            }
            catch (NoSuchElementException e) {
                log(errorMessage, "ERROR",  "text");
                Assert.fail("\n[ERROR] Failed to scroll to Element - "  + updatedXpath + " - " + e.getMessage());
            }
        }

        ElementFound(element);
    }

    private static WebElement ElementFound(WebElement element) {
        return element;
    }
    //endregion

    //region <scrollTo_ElementByText>
    public static void scrollTo_ElementByText(By element, String valueToSelect, String errorMessage) {

        try {
            List<WebElement> elmList;

            int count = 0;
            elmList = Driver.findElements(element);
            boolean found = false;

            //Getting screen size
            Dimension dim = Driver.manage().window().getSize();

            int height = dim.getHeight();
            int width = dim.getWidth();
            int x = width / 2;
            int top_y = (int) (height * 0.80);
            int bottom_y = (int) (height * 0.20);

            while (count < bottom_y && !found) {

                for (int j = 0; j < elmList.size(); j++) {

                    String value = elmList.get(j).getText();
                    System.out.println("List: " + value);

                    if (elmList.get(j).getText().equalsIgnoreCase(valueToSelect)) {
                        elmList.get(j).click();
                        found = true;
                        break;
                    }
                }

                if (!found) {
                    TouchAction action = new TouchAction(Driver);
                    action.press(PointOption.point(550, 1500));
                    action.waitAction();
                    action.moveTo(PointOption.point(550, 800));
                    action.release();
                    action.perform();

                    elmList = Driver.findElements(element);

                    count++;
                    System.out.println("\nLoop number : " + count);
                }
            }
        } catch (Exception e) {
            log(errorMessage, "ERROR", "text");
            Assert.fail("\n[ERROR] Failed to scroll to element \"" + valueToSelect + "\" and select --- " + e.getMessage());
        }
    }
    //endregion

    //region <scrollToElementXPath>
    public static void scrollToElementXPath(String elementXpath, String valueToSelect, String errorMessage) {

        try {
            do {
                try {
                    Driver.findElement(By.xpath(elementXpath)).sendKeys(valueToSelect);
                    break;
                }
                catch (Exception NoSuchElementException) {
                    Map<String, Object> params = new HashMap<>();
                    params.put("end", "40%,90%");
                    params.put("start", "40%,20%");
                    params.put("duration", "2");
                    Object res = Driver.executeScript("mobile:touch:swipe", params);
                }

            } while (true);
        }
        catch(Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to scroll to Element - "  + elementXpath + " - " + e.getMessage());
        }
    }
    //endregion

    //region <scrollToOptionElementByXpath>
    public static void scrollToOptionElementByXpath(String elementXpath, String valueToSelect, String errorMessage) {

        String updatedXpath = "";

        try {
            updatedXpath = updateXpathValueWithString(elementXpath, valueToSelect); //update default value saved in .properties

            waitForElementByXpath(updatedXpath, errorMessage);
            WebElement elmt = Driver.findElement(By.xpath(updatedXpath));
            ((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true);", elmt);
            Thread.sleep(500);

            System.out.println("Scrolled To Element - " + updatedXpath);
            Assert.assertTrue("Scrolled To Element - " + updatedXpath, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to scroll to Element - "  + updatedXpath + " - " + e.getMessage());
        }
    }
    //endregion

    //region <scrollToText>
    public static void scrollToText(String elementXpath, String valueToSelect, String errorMessage) {

        String updatedXpath = "";

        try {
            updatedXpath = updateXpathValueWithString(elementXpath, valueToSelect); //update default value saved in .properties

            JavascriptExecutor js = Driver;
            HashMap<String, String> scrollObject = new HashMap<>();
            scrollObject.put("direction", "down");
            js.executeScript("mobile: scroll", scrollObject);

            System.out.println("Scrolled To Element - " + updatedXpath);
            Assert.assertTrue("Scrolled To Element - " + updatedXpath, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to scroll To Element - "  + updatedXpath + " - " + e.getMessage());
        }
    }
    //endregion

    //region <selectFromDropDownListByXpath>
    public static void selectFromDropDownListByXpath(String dropdownlistXpath, String valueToSelect, String errorMessage) {

        try {
            waitForElementByXpath(dropdownlistXpath, errorMessage);
            Select dropDownList = new Select(Driver.findElement(By.xpath(dropdownlistXpath)));
            WebElement formXpath = Driver.findElement(By.xpath(dropdownlistXpath));
            formXpath.click();
            dropDownList.selectByVisibleText(valueToSelect);

            System.out.println("Selected Text : " + valueToSelect + " from : " + dropdownlistXpath);
            Assert.assertTrue("Selected Text : " + valueToSelect + " from : " + dropdownlistXpath, true);
        }
        catch (Exception e) {
            System.out.println("Failed to select from dropdown list by text using Xpath - " + e.getMessage());
            Assert.fail("\n[ERROR] Failed to select text : " + valueToSelect + " from dropdown list by Xpath  ---  " + e.getMessage());
        }
    }
    //endregion

    //region <selectFromDropDownList>
    public static void selectFromDropDownList(By dropdownList, String valueToSelect, String errorMessage) {

        try {
            waitForElement(dropdownList, errorMessage);
            Select dropDownList = new Select(Driver.findElement(dropdownList));
            WebElement formXpath = Driver.findElement(dropdownList);
            formXpath.click();
            dropDownList.selectByVisibleText(valueToSelect);

            System.out.println("Selected Text : " + valueToSelect + " from : " + dropdownList);
            Assert.assertTrue("Selected Text : " + valueToSelect + " from : " + dropdownList, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to select text : " + valueToSelect + " from dropdown list by Xpath  ---  " + dropdownList + " -- " + e.getMessage());
        }
    }
    //endregion

    //region <selectElementFromDropDownListByXpath>
    /*Dynamic method to select value from dropdown list*/
    public static void selectElementFromDropDownListByXpath(String dropdownListXpath, String valueToSelect, String errorMessage) {

        try {

            waitForElementByXpath(dropdownListXpath, errorMessage);
            Select dropDownList = new Select(Driver.findElement(By.xpath(dropdownListXpath)));
            WebElement formXpath = Driver.findElement(By.linkText(valueToSelect));
            formXpath.click();
            dropDownList.selectByVisibleText(valueToSelect);

            System.out.println("Selected Text : " + valueToSelect + " from : " + dropdownListXpath);
            Assert.assertTrue("Selected Text : " + valueToSelect + " from : " + dropdownListXpath, true);
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to select text : " + valueToSelect + " from dropdown list by Xpath : " + dropdownListXpath + " ---  " + e.getMessage());
        }
    }
    //endregion

    //region <selectFromDropDownList_ByXpath>
    /*Dynamic method to select value from dropdown list*/
    public static void selectFromDropDownList_ByXpath(String dropdownListXpath, String valueToSelect, String errorMessage) {

        try {
            String xp = updateXpathValueWithString(dropdownListXpath, valueToSelect); //update default value saved in .properties

            waitForElementByXpath(xp, errorMessage);
            Select dropDownList = new Select(Driver.findElement(By.xpath(xp)));
            WebElement formXpath = Driver.findElement(By.xpath(xp));
            formXpath.click();
            dropDownList.selectByVisibleText(valueToSelect);

            System.out.println("Selected Text : " + valueToSelect + " from : " + xp);
            Assert.assertTrue("Selected Text : " + valueToSelect + " from : " + xp, true);
        }
        catch (Exception e) {
            System.out.println("Failed to select from dropdown list by text using Xpath - " + e.getMessage());
            Assert.fail("\n[ERROR] Failed to select text : " + valueToSelect + " from dropdown list by Xpath  ---  " + e.getMessage());
        }
    }
    //endregion

    //region <sendKeys>
    public static void sendKeys(String choice) {

        try {
            AndroidDriver androidDriver = Driver;

            switch (choice.toUpperCase()) {
                case "TAB": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.TAB));
                    System.out.println("Pressed: \"" + choice + "\"");
                    Assert.assertTrue("Pressed: " + choice, true);
                    break;
                }
                case "ENTER": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.ENTER));
                    System.out.println("Pressed: \"" + choice + "\"");
                    Assert.assertTrue("Pressed: " + choice, true);
                    break;
                }
                case "SEARCH": {
                    androidDriver.pressKey(new KeyEvent(AndroidKey.SEARCH));
                    System.out.println("Pressed: \"" + choice + "\"");
                    Assert.assertTrue("Pressed: " + choice, true);
                    break;
                }
                case "HOME": { //Home Menu Button
                    androidDriver.pressKey(new KeyEvent(AndroidKey.HOME));
                    System.out.println("Pressed: \"" + choice + "\"");
                    Assert.assertTrue("Pressed: " + choice, true);
                    break;
                }
                case "BACK": { //Back Button
                    androidDriver.pressKey(new KeyEvent(AndroidKey.BACK));
                    System.out.println("Pressed: \"" + choice + "\"");
                    Assert.assertTrue("Pressed: " + choice, true);
                    break;
                }
            }
        }
        catch (Exception e) {
            log("Failed to send keypress: \"" + choice + "\"", "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to send keypress: \"" + choice + "\" --- " + e.getMessage());
        }
    }
    //endregion

    //region <swipe>
    public static void swipe(String direction) {

        Dimension size = Driver.manage().window().getSize();

        int startX = 0;
        int endX = 0;
        int startY = 0;
        int endY = 0;

        switch (direction.toUpperCase()){
            case "RIGHT":
                startY = size.height /2;
                startX = (int) (size.width * 0.90);
                endX = (int) (size.width * 0.05);

                break;
            case "LEFT":
                startY = size.height /2;
                startX = (int) (size.width * 0.05);
                endX = (int) (size.width * 0.90);

                break;
            case "UP":
                endY= (int) (size.height * 0.70);
                startY  = (int) (size.height * 0.30);
                startX = (size.width / 2);

                break;
            case "DOWN":
                startY = (int) (size.height * 0.70);
                endY = (int) (size.height * 0.30);
                startX = (size.width / 2);

                break;
        }

        TouchAction action = new TouchAction(Driver);
        action.press(PointOption.point(startX, startY));
        action.waitAction();
        action.moveTo(PointOption.point(endX, startY));
        action.release();
        action.perform();
    }
    //endregion

    //region <swipeDown>
    public static void swipeDown(String errorMessage) throws InterruptedException {

        try {
            Driver.context("NATIVE_APP");
            Dimension dimensions = Driver.manage().window().getSize();

            Double screenHeightStart = dimensions.getHeight() * 0.1;
            int tempStart = screenHeightStart.intValue();
            int scrollStart = tempStart / 4;
            System.out.println("start = " + scrollStart);

            Double screenHeightEnd = dimensions.getHeight() * 0.5;
            int scrollEnd = screenHeightEnd.intValue();
            System.out.println("End = " + scrollEnd);
        }
        catch(Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to swipe down" + e.getMessage());
        }
    }

    public static void swipeDown() throws InterruptedException {

        try {
            Driver.context("NATIVE_APP");
            Dimension dimensions = Driver.manage().window().getSize();

            Double screenHeightStart = dimensions.getHeight() * 0.7;
            int scrollStart = screenHeightStart.intValue();
            System.out.println("start = " + scrollStart);

            Double screenHeightEnd = dimensions.getHeight() * 0.2;
            int scrollEnd = screenHeightEnd.intValue();
            System.out.println("End = " + scrollEnd);
        }
        catch(Exception e) {
            log("Failed to swipe down", "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to swipe down" + e.getMessage());
        }
    }
    //endregion

    //region <swipeUp>
    public static void swipeUp(String errorMessage) throws InterruptedException {

        try {
            Driver.context("NATIVE_APP");
            Dimension dimensions = Driver.manage().window().getSize();
            Double screenHeightStart = dimensions.getHeight() * 0.3;
            int scrollStart = screenHeightStart.intValue();
            System.out.println("start = " + scrollStart);

            Double screenHeightEnd = dimensions.getHeight() * 0.7;
            int scrollEnd = screenHeightEnd.intValue();
            System.out.println("End = " + scrollEnd);
        }
        catch(Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to swipe up" + e.getMessage());
        }
    }

    public static void swipeUp(int x, int y) {

        try {
            Driver.context("NATIVE_APP");
            Dimension dimensions = Driver.manage().window().getSize();
            Double screenHeightStart = dimensions.getHeight() * 0.3;
            int scrollStart = screenHeightStart.intValue();
            System.out.println("start = " + scrollStart);
            Double screenHeightEnd = dimensions.getHeight() * 0.7;
            int scrollEnd = screenHeightEnd.intValue();
            System.out.println("End = " + scrollEnd);
        }
        catch(Exception e) {
            Assert.fail("\n[ERROR] Failed to swipe up - " + e.getMessage());
        }
    }

    public static void swipeUp() {

        try {
            Driver.context("NATIVE_APP");
            Dimension dimensions = Driver.manage().window().getSize();
            Double screenHeightStart = dimensions.getHeight() * 0.3;
            int scrollStart = screenHeightStart.intValue();
            System.out.println("start = " + scrollStart);
            Double screenHeightEnd = dimensions.getHeight() * 0.7;
            int scrollEnd = screenHeightEnd.intValue();
            System.out.println("End = " + scrollEnd);
        }
        catch(Exception e) {
            Assert.fail("\n[ERROR] Failed to swipe up - " + e.getMessage());
        }
    }

    public static void swipeUp(String elementXpath, String valueToSelect, String errorMessage) {

        String updatedXpath;

        try {
            updatedXpath = updateXpathValueWithString(elementXpath, valueToSelect); //update default value saved in .properties

            waitForElementByXpath(updatedXpath, errorMessage);
            Driver.context("NATIVE_APP");
            Dimension dimensions = Driver.manage().window().getSize();
            Double screenHeightStart = dimensions.getHeight() * 0.3;
            int scrollStart = screenHeightStart.intValue();
            System.out.println("start = " + scrollStart);
            Double screenHeightEnd = dimensions.getHeight() * 0.7;
            int scrollEnd = screenHeightEnd.intValue();
            System.out.println("End = " + scrollEnd);
        }
        catch(Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to swipe up to Element - "  + elementXpath + " - " + e.getMessage());
        }
    }

    public static void swipeUp(String elementXpath, String errorMessage) {

        try {
            waitForElementByXpath(elementXpath, errorMessage);

            Driver.context("NATIVE_APP");
            Dimension dimensions = Driver.manage().window().getSize();
            Double screenHeightStart = dimensions.getHeight() * 0.3;
            int scrollStart = screenHeightStart.intValue();
            System.out.println("start = " + scrollStart);
            Double screenHeightEnd = dimensions.getHeight() * 0.7;
            int scrollEnd = screenHeightEnd.intValue();
            System.out.println("End = " + scrollEnd);
        }
        catch(Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to swipe up to Element - "  + elementXpath + " - " + e.getMessage());
        }
    }
    //endregion

    //region <switchToContextByName>
    public static void switchToContextByName(String contextName) {

        try {
            Set<String> contextNames = Driver.getContextHandles();
            System.out.println("All context - " + contextNames);

            for (String cont : contextNames) {
                if (cont.contains(contextName)) {
                    Driver.context(cont);
                    System.out.println("Switched to context - " + cont);
                }
            }
        }
        catch(Exception e) {
            Assert.fail("\n[ERROR] Failed to switch to context By Name  ---  " + e.getMessage());
        }
    }
    //endregion

    //region <To update an Xpath value from the .properties file>
    public static String updateXpathValueWithString(String xp, String value) {
        return xp.replace("value", value);
    }
    //endregion

    //region <validateSubstringIsPresent>
    public static void validateSubstringIsPresent(String string, String substring, String errorMessage) {

        boolean isFound = string.contains(substring);
        if (isFound) {
            System.out.println("Substring: \"" + substring + "\" is present");
            Assert.assertTrue("Substring: \"" + substring + "\" is present", true);
        }
        else {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Substring: \"" + substring + "\" is not present");
        }
    }
    //endregion

    //region <validateTextFromExtractedListElement>
    /**
     * This method is used to validate that a list of extracted texts
     * contains a specified substring
     */
    public static void validateTextFromExtractedListElement(By element, String textToValidate, String errorMessage) {

        try {
            List<WebElement> elmList;

            int count = 0;
            elmList = Driver.findElements(element);
            boolean found = false;

            //Getting screen size
            Dimension dim = Driver.manage().window().getSize();

            int height = dim.getHeight();
            int width = dim.getWidth();
            int x = width / 2;

            int top_y = (int) (height * 0.80);
            int bottom_y = (int) (height * 0.20);

            while (count < 5 && !found) {
                for (int i = 0; i < elmList.size(); i++) {
                    if (elmList.get(i).getText().contains(textToValidate)) {
                        found = true;
                        Assert.assertTrue("Substring: \"" + textToValidate + "\" is present", true);
                        break;
                    }
                }

                if (!found) {
                    TouchAction action = new TouchAction(Driver);

                    action.press(PointOption.point(x, top_y)); //(x, y) coordinate for the starting point of scrolling
                    action.waitAction();
                    action.moveTo(PointOption.point(x, bottom_y)); //(x, y) coordinate for the ending point of scrolling
                    action.release();
                    action.perform();

                    elmList = Driver.findElements(element);
                    count++;
                }
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to validate text: \"" + textToValidate + "\" is present in the list --- " + e.getMessage());
        }
    }
    //endregion

    //region <validateTextFromExtractedListElementAndClick>
    /**
     * This method is used to validate that a list of extracted texts
     * contains a specified substring, then click it
     */
    public static void validateTextFromExtractedListElementAndClick(By element, String textToValidate, String errorMessage) {

        try {
            List<WebElement> elmList;

            int count = 0;
            elmList = Driver.findElements(element);
            boolean found = false;

            //Getting screen size
            Dimension dim = Driver.manage().window().getSize();

            int height = dim.getHeight();
            int width = dim.getWidth();
            int x = width / 2;

            int top_y = (int) (height * 0.80);
            int bottom_y = (int) (height * 0.20);

            while (count < 5 && !found) {
                for (int i = 0; i < elmList.size(); i++) {
                    if (elmList.get(i).getText().contains(textToValidate)) { //check if contains substring
                        waitForElementToBeDisplayed(elmList.get(i), 5); //wait for value to be clicked to be displayed
                        elmList.get(i).click(); //click it
                        found = true;
                        Assert.assertTrue("Substring: \"" + textToValidate + "\" is present and clicked", true);
                        break;
                    }
                }

                if (!found) {
                    TouchAction action = new TouchAction(Driver);

                    action.press(PointOption.point(x, top_y)); //(x, y) coordinate for the starting point of scrolling
                    action.waitAction();
                    action.moveTo(PointOption.point(x, bottom_y)); //(x, y) coordinate for the ending point of scrolling
                    action.release();
                    action.perform();

                    elmList = Driver.findElements(element);
                    count++;
                }
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to validate text: \"" + textToValidate + "\" is present in the list and clicked --- " + e.getMessage());
        }
    }
    //endregion

    //region <validateTextFromExtractedListElementAndLongPress>
    /**
     * This method is used to validate that a list of extracted texts
     * contains a specified substring
     */
    public static void validateTextFromExtractedListElementAndLongPress(By element, String textToValidate, String errorMessage) {

        try {
            List<WebElement> elmList;

            int count = 0;
            elmList = Driver.findElements(element);
            boolean found = false;

            //Getting screen size
            Dimension dim = Driver.manage().window().getSize();

            int height = dim.getHeight();
            int width = dim.getWidth();
            int x = width / 2;

            int top_y = (int) (height * 0.80);
            int bottom_y = (int) (height * 0.20);

            while (count < 5 && !found) {
                for (int i = 0; i < elmList.size(); i++) {
                    if (elmList.get(i).getText().contains(textToValidate)) {
                        found = true;
                        pressAndHoldReturn(textToValidate, errorMessage);
                        Assert.assertTrue("Substring: \"" + textToValidate + "\" is present", true);
                        break;
                    }
                }

                if (!found) {
                    TouchAction action = new TouchAction(Driver);

                    action.press(PointOption.point(x, top_y)); //(x, y) coordinate for the starting point of scrolling
                    action.waitAction();
                    action.moveTo(PointOption.point(x, bottom_y)); //(x, y) coordinate for the ending point of scrolling
                    action.release();
                    action.perform();

                    elmList = Driver.findElements(element);
                    count++;
                }
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to validate text: \"" + textToValidate + "\" is present in the list --- " + e.getMessage());
        }
    }
    //endregion

    //region <waitForElement>
    public static void waitForElement(By element, String errorMessage) {

        boolean elementFound = false;
        try {
            int waitCount = 0;
            while (!elementFound && waitCount < WaitTimeout) {
                try {
                    WebDriverWait wait = new WebDriverWait(Driver, 2);

                    wait.until(ExpectedConditions.presenceOfElementLocated(element));
                    wait.until(ExpectedConditions.elementToBeClickable(element));
                    if (wait.until(ExpectedConditions.visibilityOfElementLocated(element)) != null) {
                        elementFound = true;
                        System.out.println("Found element : " + element);
                        break;
                    }
                }
                catch (Exception e) {
                    elementFound = false;
                    log("Did Not Find element: " + element + "' - " + e.getMessage(), "ERROR",  "text");
                }
                waitCount++;
            }
            if (waitCount == WaitTimeout) {
                returnElementFound(elementFound);
                log(errorMessage, "ERROR",  "text");
                Assert.fail("\n[ERROR] Reached TimeOut whilst waiting for element --- " + element);
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to wait for element --- " + element);
        }

        returnElementFound(elementFound);
    }

    private static boolean returnElementFound(boolean elementFound) {
        return elementFound;
    }
    //endregion

    //region <waitForElement With Timeout>
    /**
     * This method allows you to wait for an element By, and specify explicitly
     * the waiting time in Seconds, without using Assert.fail()
     * */
    public static boolean waitForElement(By element, Integer timeout, String errorMessage) {

        boolean elementFound = false;
        try {
            int waitCount = 0;
            Driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

            while (!elementFound && waitCount < timeout) {
                try {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(element)) != null) {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e) {
                    elementFound = false;
                }
                waitCount++;
            }
        }
        catch (Exception e) {
            elementFound = false;
            log(errorMessage, "ERROR",  "text");
        }

        Driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        return elementFound;
    }

    public static boolean waitForElement(By element, Integer timeout) {

        boolean elementFound = false;
        try {
            int waitCount = 0;
            Driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

            while (!elementFound && waitCount < timeout) {
                try {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(element)) != null) {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e) {
                    elementFound = false;
                }
                waitCount++;
            }
        }
        catch (Exception e) {
            elementFound = false;
            log("\n[ERROR] Failed to wait for element --- " + element, "ERROR",  "text");
        }

        Driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        return elementFound;
    }
    //endregion

    //region <waitForElementByXpath>
    public static void waitForElementByXpath(String elementXpath, String errorMessage) {

        boolean elementFound = false;
        try {
            int waitCount = 0;
            while (!elementFound && waitCount < WaitTimeout) {
                try {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);

                    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath)));
                    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(elementXpath)));
                    if (wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elementXpath))) != null) {
                        elementFound = true;
                        System.out.println("Found element by Xpath : " + elementXpath);
                        Assert.assertTrue("Found element by Xpath : " + elementXpath, true);
                        break;
                    }
                }
                catch (Exception e) {
                    elementFound = false;
                    log("\nDid Not Find element by Xpath : " + elementXpath, "ERROR",  "text");
                }
                waitCount++;
            }
            if (waitCount == WaitTimeout) {
                GetElementFound1(elementFound);
                log(errorMessage, "ERROR",  "text");
                Assert.fail("\n[ERROR] Reached TimeOut whilst waiting for element by Xpath --- " + elementXpath);
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to wait for element by Xpath --- " + elementXpath + "' - " + e.getMessage());
        }

        GetElementFound(elementFound);
    }

    private static boolean GetElementFound(boolean elementFound) {
        return elementFound;
    }

    private static boolean GetElementFound1(boolean elementFound) {
        return elementFound;
    }
    //endregion

    //region <waitForElementToBeDisplayed>
    /**
     * Method to wait for an element to be Displayed. Mostly used in 'if statement' to check
     * if an element is present, without using Assert.fail()
     * */
    public static boolean waitForElementToBeDisplayed(By element, Integer timeout, String errorMessage) {

        boolean elementDisplayed = false;
        try {
            int waitCount = 0;
            Driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            while (!elementDisplayed && waitCount < timeout) {
                try {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);

                    if (Driver.findElement(element).isDisplayed()) {
                        elementDisplayed = true;
                        break;
                    }
                }
                catch (Exception e) {
                    elementDisplayed = false;
                }
                waitCount++;
            }
        }
        catch (Exception e) {
            elementDisplayed = false;
            log(errorMessage, "ERROR",  "text");
        }

        Driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        return elementDisplayed;
    }

    public static boolean waitForElementToBeDisplayed(String elementXpath, String valueToWaitFor, Integer timeout, String errorMessage) {

        boolean elementDisplayed = false;
        String updatedXpath = "";
        try {
            int waitCount = 0;
            Driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            while (!elementDisplayed && waitCount < timeout) {
                try {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    updatedXpath = updateXpathValueWithString(elementXpath, valueToWaitFor); //update default value saved in .properties

                    if (Driver.findElement(By.xpath(updatedXpath)).isDisplayed()) {
                        elementDisplayed = true;
                        break;
                    }
                }
                catch (Exception e) {
                    elementDisplayed = false;
                }
                waitCount++;
            }
        }
        catch (Exception e) {
            elementDisplayed = false;
            log(errorMessage, "ERROR",  "text");
        }

        Driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        return elementDisplayed;
    }

    public static boolean waitForElementToBeDisplayed(WebElement element, Integer timeout) {

        boolean elementDisplayed = false;
        try {
            int waitCount = 0;
            Driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
            while (!elementDisplayed && waitCount < timeout)
            {
                try {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);

                    if (element.isDisplayed())
                    {
                        elementDisplayed = true;
                        break;
                    }
                }
                catch (Exception e) {
                    elementDisplayed = false;
                }
                waitCount++;
            }
        }
        catch (Exception e) {
            elementDisplayed = false;
            log("Failed to wait for element --- \"" + element, "ERROR",  "text");
        }

        Driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        return elementDisplayed;
    }
    //endregion

    //region <waitForElementBoolean>
    /**
     * Method to wait for an element's value. Mostly used in 'if statement' to check
     * if an element is present, without using Assert.fail()
     * */
    public static boolean waitForElementBoolean(By element, String errorMessage) {

        boolean elementFound = false;
        try {
            int waitCount = 0;
            Driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

            while (!elementFound && waitCount < WaitTimeout) {
                try {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(element)) != null) {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e) {
                    elementFound = false;
                }
                waitCount++;
            }
        }
        catch (Exception e) {
            elementFound = false;
            log(errorMessage, "ERROR",  "text");
        }

        Driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        return elementFound;
    }

    public static boolean waitForElementBoolean(By element, Integer timeout, String errorMessage) {

        boolean elementFound = false;
        try {
            int waitCount = 0;
            Driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

            while (!elementFound && waitCount < timeout) {
                try {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    if (wait.until(ExpectedConditions.presenceOfElementLocated(element)) != null) {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e) {
                    elementFound = false;
                }
                waitCount++;
            }
        }
        catch (Exception e) {
            elementFound = false;
            log(errorMessage, "ERROR",  "text");
        }

        Driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        return elementFound;
    }
    //endregion

    //region <waitForElementToBeClickable>
    public static void waitForElementToBeClickable(By element, Integer timeout, String errorMessage) {

        boolean elementFound = false;
        try {
            int waitCount = 0;
            while (!elementFound && waitCount < timeout) {
                try {
                    WebDriverWait wait = new WebDriverWait(Driver, 2);

                    wait.until(ExpectedConditions.presenceOfElementLocated(element));
                    wait.until(ExpectedConditions.elementToBeClickable(element));
                    if (wait.until(ExpectedConditions.visibilityOfElementLocated(element)) != null) {
                        elementFound = true;
                        System.out.println("Found element : " + element);
                        break;
                    }
                }
                catch (Exception e) {
                    elementFound = false;
                }
                waitCount++;
            }
            if (waitCount == WaitTimeout) {
                returnElementFound(elementFound);
                log(errorMessage, "ERROR",  "text");
                Assert.fail("\n[ERROR] Reached TimeOut whilst waiting for element --- " + element);
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to wait for element --- " + element);
        }

        returnElementFound(elementFound);
    }
    //endregion

    //region <waitForOption>
    /**
     * Dynamic boolean method to wait for an element's value. Mostly used in 'if statement' to check
     * if an element is present, without using Assert.fail()
     * */
    public static boolean waitForOption(String elementXpath, String valueToWaitFor) {

        boolean elementFound = false;
        String updatedXpath;

        Driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

        try {
            WebDriverWait wait = new WebDriverWait(Driver, 1);
            updatedXpath = updateXpathValueWithString(elementXpath, valueToWaitFor); //update default value saved in .properties

            if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(updatedXpath))) != null) {
                elementFound = true;
            }
        }
        catch (Exception e) {
            elementFound = false;
            log("Failed to wait for element by Xpath --- " + elementXpath, "ERROR",  "text");
        }

        Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        return elementFound;
    }

    public static boolean waitForOption(String elementXpath, Integer timeout) {

        boolean elementFound = false;
        try {
            int waitCount = 0;
            Driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

            while (!elementFound && waitCount < timeout) {
                try {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);

                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(elementXpath))) != null) {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e) {
                    elementFound = false;
                }

                waitCount++;
            }
        }
        catch (Exception e) {
            elementFound = false;
            log("Failed to wait for element by Xpath --- " + elementXpath, "ERROR",  "text");
        }

        Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        return elementFound;
    }

    public static boolean waitForOption(String elementXpath, String valueToWaitFor, Integer timeout) {

        boolean elementFound = false;
        String updatedXpath = "";
        try {
            int waitCount = 0;
            Driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

            while (!elementFound && waitCount < timeout) {
                try {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    updatedXpath = updateXpathValueWithString(elementXpath, valueToWaitFor); //update default value saved in .properties

                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(updatedXpath))) != null) {
                        elementFound = true;
                        break;
                    }
                }
                catch (Exception e) {
                    elementFound = false;
                }

                waitCount++;
            }
        }
        catch (Exception e) {
            elementFound = false;
            log("Failed to wait for element by Xpath --- " + updatedXpath, "ERROR",  "text");
        }

        Driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

        return elementFound;
    }
    //endregion

    //region <waitForOptionByXpath>
    /*Dynamic method to wait for an element Xpath. Using a dynamic Xpath of the element, we just specify the value to select*/
    public static void waitForOptionByXpath(String elementXpath, String valueToWaitFor, String errorMessage) {

        boolean elementFound = false;
        String updatedXpath = "";
        try {
            int waitCount = 0;
            while (!elementFound && waitCount < WaitTimeout) {
                try {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    updatedXpath = updateXpathValueWithString(elementXpath, valueToWaitFor); //update default value saved in .properties

                    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(updatedXpath)));
                    wait.until(ExpectedConditions.elementToBeClickable(By.xpath(updatedXpath)));
                    if (wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(updatedXpath))) != null) {
                        elementFound = true;
                        log("Found element by Xpath: " + updatedXpath, "INFO",  "text");
                        break;
                    }
                }
                catch (Exception e) {
                    elementFound = false;
                }
                waitCount++;
            }
            if (waitCount == WaitTimeout) {
                GetElementFound1(elementFound);
                log(errorMessage, "ERROR",  "text");
                Assert.fail("\n[ERROR] Reached TimeOut whilst waiting for element by Xpath --- " + updatedXpath);
            }
        }
        catch (Exception e) {
            log(errorMessage, "ERROR",  "text");
            Assert.fail("\n[ERROR] Failed to wait for element by Xpath --- " + updatedXpath + "' - " + e.getMessage());
        }

        GetElementFound(elementFound);
    }

    /**
     * Dynamic boolean method to wait for an element's value. Mostly used in 'if statement' to check
     * if an element is present, without using Assert.fail()
     * */
    public static boolean waitForOptionByXpath(String elementXpath, String valueToWaitFor, Integer timeout) {

        boolean elementFound = false;
        String updatedXpath = "";
        try {
            int waitCount = 0;
            Driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);

            while (!elementFound && waitCount < timeout) {
                try {
                    WebDriverWait wait = new WebDriverWait(Driver, 1);
                    updatedXpath = updateXpathValueWithString(elementXpath, valueToWaitFor); //update default value saved in .properties

                    if (wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(updatedXpath))) != null) {
                        elementFound = true;
                        log("Found element by Xpath: " + updatedXpath, "INFO",  "text");
                        break;
                    }
                }
                catch (Exception e) {
                    elementFound = false;
                }
                waitCount++;
            }
        }
        catch (Exception e) {
            elementFound = false;
            log("Failed to wait for element by Xpath --- \"" + updatedXpath, "ERROR",  "text");
        }

        Driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

        return elementFound;
    }
    //endregion
}

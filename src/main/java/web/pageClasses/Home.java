package main.java.web.pageClasses;

/**
 * @author lionel.mangoua
 * date: 25/01/22
 */

import main.java.engine.DriverFactory;

public class Home extends DriverFactory {

//    public static void navigateToUrl(String URL) {
//
//        navigateToUrl(URL);
//
//        log("Navigated to " + url + " successfully","INFO",  "text");
//    }

    //region <navigateToUrl>
    public static void navigateToUrl(String URL) {

        driver.get(URL);

        log("Navigated to " + URL + " successfully","INFO",  "text");
    }
    //endregion
}

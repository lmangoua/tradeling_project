package testSuites;

/**
 * @author lionel.mangoua
 * date: 26/01/22
 */

import main.java.engine.DriverFactory;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

public class testCasesMobile extends DriverFactory {

    @Features("TRADELING TEST CASE 1")
    @Description("* Navigate to Home page")
    @Stories("Test 1")
    @Test
    public void NavigateToHomePage() {

//        Home.navigateToUrl(url);
//
//        SeleniumUtility.attachScreenShot("NavigateToHomePage test executed successfully");

        log("\n=============== NavigateToHomePage test executed successfully ===============\n","INFO",  "text");
    }
}
